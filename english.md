| | Labial | Dental | Alveolar | Post-alveolar | Palatal | Velar | Glottal
| - | - | - | - | - | - | - | -
| Nasal | m | | n | | | ŋ
| Stop tense | p | | t | tʃ = c | | k
| Stop lax | b | | d | dʒ = j | | ɡ
| Fricative tense | f | θ = þ | s | ʃ = š | | | h
| Fricative lax | v | ð | z | ʒ = ž
| Approximant | | | l | r | j = y | w = v

| | Front | Central | Back
| - | - | - | -
| Close | ɪ = i | ʊ = u
| Mid | e | ə = ə | ɒ = o
| Open | æ | ɐ = a

| | Front | Central | Back
| - | - | - | -
| Close | iː = ii | uː = uu | ɔː = oo
| Mid | ɛː = ee | ɜː = əə
| Open |  |  | ɑː = aa
